import React from 'react';

const Details = ({title, userId, id}) => {
  return (<div>
    <div>
      <label>Title: </label>
      <span>{title}</span>
    </div>
    <div>
      <label>UserId: </label>
      <span>{userId}</span>
    </div>
    <div>
      <label>Id: </label>
      <span>{id}</span>
    </div>
  </div>)
};

export default Details;